$(function() {
  $(document).on('click tap', '.dropdown-menu', function(e) {
    if ($(this).hasClass('keep-open-on-click')) { e.stopPropagation(); }
  });
  $('.js-mobile-menu__user-btn').on('click tap', function() {
    $('body').removeClass('mobile-menu__site-nav-active');
    $('body').toggleClass('mobile-menu__user-active');
  });
  $('.js-mobile-menu__site-nav-btn').on('click tap', function() {
    $('body').removeClass('mobile-menu__user-active');
    $('body').toggleClass('mobile-menu__site-nav-active');
  });
  $('.js-site-nav__list-link').on('click tap', function() {
    $(this).parent().siblings().removeClass('mega-menu_active');
    $(this).parent().toggleClass('mega-menu_active');
    return false;
  });

  var fileInput = $('.custom-file-input');

  fileInput.change(function() {
    $this = $(this);
    $('.custom-file-control').text($this.val());
  });

  $('.custom-file-control').on('click tap', function() {
    fileInput.click();
  }).show();

  $('.js-input-btn').on('click', function() {
    $(this).addClass('active').siblings('.js-input-btn').removeClass('active');
    $(this).find('.custom-radio input').prop("checked", "checked");
  });
});

$(document).ready(function() {
  $(function() {
    $('[data-toggle="tooltip"]').tooltip();
  });
  $(function() {
    $('[data-toggle="popover"]').popover();
  });
});

jQuery(document).ready(function() {
  // This button will increment the value
  $('.js-qtyplus').click(function(e) {
    // Stop acting like a button
    e.preventDefault();
    // Get the field name
    fieldName = $(this).attr('field');
    // Get its current value
    var currentVal = parseInt($('input[name=' + fieldName + ']').val());
    // If is not undefined
    if (!isNaN(currentVal)) {
      // Increment
      $('input[name=' + fieldName + ']').val(currentVal + 1);
    } else {
      // Otherwise put a 0 there
      $('input[name=' + fieldName + ']').val(0);
    }
  });
  // This button will decrement the value till 0
  $(".js-qtyminus").click(function(e) {
    // Stop acting like a button
    e.preventDefault();
    // Get the field name
    fieldName = $(this).attr('field');
    // Get its current value
    var currentVal = parseInt($('input[name=' + fieldName + ']').val());
    // If it isn't undefined or its greater than 0
    if (!isNaN(currentVal) && currentVal > 0) {
      // Decrement one
      $('input[name=' + fieldName + ']').val(currentVal - 1);
    } else {
      // Otherwise put a 0 there
      $('input[name=' + fieldName + ']').val(0);
    }
  });
});

$(document).ready(function() {
  $(".js-owl-full-width").owlCarousel({
    navigation: false,
    items: 1,
    autoplay: true,
    autoplayHoverPause: true,
    loop: true,
    margin: 10
  });
});

$(document).ready(function() {

  var sync1 = $(".js-big-images");
  var sync2 = $(".js-thumbs");
  var syncedSecondary = true;

  sync1.owlCarousel({
    items: 1,
    // slideSpeed: 2000,
    nav: false,
    // autoplay: true,
    dots: false,
    loop: true,
    margin: 10,
    responsiveRefreshRate: 200
  }).on('changed.owl.carousel', syncPosition);

  sync2
    .on('initialized.owl.carousel', function() {
      sync2.find(".owl-item").eq(0).addClass("current");
    })
    .owlCarousel({
      dots: false,
      nav: true,
      // smartSpeed: 200,
      // slideSpeed: 500,
      responsiveRefreshRate: 100,
      margin: 10,
      responsive: {
        0: {
          items: 4,
          slideBy: 4
        },
        600: {
          items: 3,
          slideBy: 3
        },
        800: {
          items: 4,
          slideBy: 4
        }
      },
      navText: [
        '<i class="eh-icon-arrow-left"></i>',
        '<i class="eh-icon-arrow-right"></i>'
      ]
    }).on('changed.owl.carousel', syncPosition2);

  function syncPosition(el) {
    //if you set loop to false, you have to restore this next line
    //var current = el.item.index;

    //if you disable loop you have to comment this block
    var count = el.item.count - 1;
    var current = Math.round(el.item.index - (el.item.count / 2) - .5);

    if (current < 0) {
      current = count;
    }
    if (current > count)  {
      current = 0;
    }

    //end block

    sync2
      .find(".owl-item")
      .removeClass("current")
      .eq(current)
      .addClass("current");
    var onscreen = sync2.find('.owl-item.active').length - 1;
    var start = sync2.find('.owl-item.active').first().index();
    var end = sync2.find('.owl-item.active').last().index();

    if (current > end) {
      sync2.data('owl.carousel').to(current, 300, true);
    }
    if (current < start) {
      sync2.data('owl.carousel').to(current - onscreen, 300, true);
    }
  }

  function syncPosition2(el) {
    if (syncedSecondary) {
      var number = el.item.index;
      sync1.data('owl.carousel').to(number, 100, true);
    }
  }

  sync2.on("click", ".owl-item", function(e) {
    e.preventDefault();
    var number = $(this).index();
    sync1.data('owl.carousel').to(number, 300, true);
  });
});



$(function() {
  // Placeholder fix for IE
  $('.lt-ie10 [placeholder]').focus(function() {
    var i = $(this);
    if (i.val() == i.attr('placeholder')) {
      i.val('').removeClass('placeholder');
      if (i.hasClass('password')) {
        i.removeClass('password');
        this.type = 'password';
      }
    }
  }).blur(function() {
    if (i.val() == '' || i.val() == i.attr('placeholder')) {
      if (this.type == 'password') {
        i.addClass('password');
        this.type = 'text';
      }
      i.addClass('placeholder').val(i.attr('placeholder'));
    }
  }).blur().parents('form').submit(function() {
    //if($(this).validationEngine('validate')) { // If using validationEngine
    $(this).find('[placeholder]').each(function() {
      var i = $(this);
      if (i.val() == i.attr('placeholder'))
        i.val('');
      i.removeClass('placeholder');
    });
    //}
  });
});
